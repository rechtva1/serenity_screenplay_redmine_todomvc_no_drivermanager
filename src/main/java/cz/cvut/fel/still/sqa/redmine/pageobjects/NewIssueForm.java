package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Vaclav Rechtberger
 */
public class NewIssueForm //extends PageObject
{
    public static Target NEW_ISSUE_SUBJECT = Target.the("New issue subject field.").located(By.id("issue_subject"));
    public static Target NEW_ISSUE_DESCRIPTION = Target.the("New issue description field.").located(By.id("issue_description"));
    public static Target SUBMIT = Target.the("Submit new project form").locatedBy("//input[@type='submit' and @name='commit']");
}
