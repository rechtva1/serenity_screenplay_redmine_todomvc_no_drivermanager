package cz.cvut.fel.still.sqa.todos.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


/**
 * @author Vaclav Rechtberger
 */
public class TodoList extends PageObject {
    public static final Target WHAT_NEEDS_TO_BE_DONE = Target.the("'What needs to be done?' field").locatedBy("*.new-todo");

    public static final Target LIST_ITEMS = Target
            .the("List of todo items")
            .locatedBy("*.ng-binding");
}
