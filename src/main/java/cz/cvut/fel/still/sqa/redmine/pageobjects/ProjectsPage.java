package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Vaclav Rechtberger
 */
public class ProjectsPage extends PageObject {
    public static Target NEW_PROJECT_LINK = Target.the("New project link").located(By.xpath("//a[@href='/projects/new']"));
}
