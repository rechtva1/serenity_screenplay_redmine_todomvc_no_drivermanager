package cz.cvut.fel.still.sqa.redmine.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static cz.cvut.fel.still.sqa.redmine.pageobjects.TopMenu.ACTIVE_USER;


/**
 * @author Vaclav Rechtberger
 */
public class ActiveUserLogin implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(ACTIVE_USER)
                .viewedBy(actor).asString();
    }

    public static Question<String> is() {
        return new ActiveUserLogin();
    }
}
