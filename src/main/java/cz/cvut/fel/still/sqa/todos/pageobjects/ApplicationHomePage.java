package cz.cvut.fel.still.sqa.todos.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

/**
 * @author Vaclav Rechtberger
 */
@DefaultUrl("http://todomvc.com/examples/angularjs/")
public class ApplicationHomePage extends PageObject {
}