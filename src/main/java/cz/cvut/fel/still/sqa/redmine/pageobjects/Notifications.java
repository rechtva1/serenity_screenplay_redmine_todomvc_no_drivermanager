package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Vaclav Rechtberger
 */
public class Notifications extends PageObject {
    public static Target SUCCESS_NOTIFICATION = Target.the("Success notification").located(By.id("flash_notice"));
}
