package cz.cvut.fel.still.sqa.redmine.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.thucydides.core.annotations.Step;

import static cz.cvut.fel.still.sqa.redmine.pageobjects.HeaderMenu.PROJECTS_SELECT_BOX;
import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Vaclav Rechtberger
 */
public class SelectAnProject implements Task {
    private String name;

    public SelectAnProject(String name) {
        this.name = name;
    }

    @Step("{0} selects project named '#name'")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SelectFromOptions.byVisibleText(name).from(PROJECTS_SELECT_BOX)
        );
    }

    public static SelectAnProject named(String name) {
        return instrumented(SelectAnProject.class, name);
    }
}