package cz.cvut.fel.still.sqa.redmine;

import cz.cvut.fel.still.sqa.redmine.questions.SuccessMessage;
import cz.cvut.fel.still.sqa.redmine.tasks.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;

/**
 * @author Vaclav Rechtberger
 */
@RunWith(SerenityRunner.class)
public class InsertNewBug {
    Actor student = Actor.named("Student");
    String login;
    String password;
    String email;
    String passwordConfirmation;
    String firstName;
    String lastName;
    String projectName;
    String bugName;
    String bugDescription;


    @Managed
    WebDriver theBrowser;// = driver;

    @Before
    public void studentCanBrowseTheWeb(){
        long nanoTime = System.nanoTime();
        login = "Name" + nanoTime;
        password = "123456789";
        passwordConfirmation = password;
        firstName = "X";
        lastName = "Y";
        email = login + "@xxx.com";
        projectName = login + "'s project";
        bugName = "NEW BUG";
        bugDescription = "DESCRIPTION";

        givenThat(student).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_insert_new_bug() {
        givenThat(student).wasAbleTo(
                StartOn.aLoggedoutPage(),
                CreateAnAccount.withCredentials(login, password, passwordConfirmation, firstName, lastName, email),
                CreateAnProject.named(projectName),
                SelectAnProject.named(projectName)
        );
        when(student).attemptsTo(InsertBug.describedWith(bugName,bugDescription));

        then(student).should(seeThat(SuccessMessage.text(), not(isEmptyOrNullString())));
    }
}
