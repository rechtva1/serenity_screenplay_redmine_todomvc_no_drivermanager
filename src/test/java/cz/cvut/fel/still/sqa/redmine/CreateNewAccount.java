package cz.cvut.fel.still.sqa.redmine;

import cz.cvut.fel.still.sqa.redmine.questions.ActiveUserLogin;
import cz.cvut.fel.still.sqa.redmine.tasks.CreateAnAccount;
import cz.cvut.fel.still.sqa.redmine.tasks.StartOn;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author Vaclav Rechtberger
 */
@RunWith(SerenityRunner.class)
public class CreateNewAccount {
    Actor student = Actor.named("Student");
    String login;
    String password;
    String email;
    String passwordConfirmation;
    String firstName;
    String lastName;


    @Managed
    WebDriver theBrowser;// = driver;

    @Before
    public void studentCanBrowseTheWeb(){
        login = "Name" + System.nanoTime();
        password = "123456789";
        passwordConfirmation = password;
        firstName = "X";
        lastName = "Y";
        email = login + "@xxx.com";
        givenThat(student).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_create_new_account() {
        givenThat(student).wasAbleTo(StartOn.aLoggedoutPage());
        when(student).attemptsTo(CreateAnAccount.withCredentials(login, password, passwordConfirmation, firstName, lastName, email));
        then(student).should(seeThat(ActiveUserLogin.is(), equalTo(login)));
    }
}
