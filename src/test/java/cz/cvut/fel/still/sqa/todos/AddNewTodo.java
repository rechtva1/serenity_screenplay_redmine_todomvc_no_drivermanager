package cz.cvut.fel.still.sqa.todos;

import cz.cvut.fel.still.sqa.todos.questions.TodoItemsList;
import cz.cvut.fel.still.sqa.todos.tasks.AddATodoItem;
import cz.cvut.fel.still.sqa.todos.tasks.StartWith;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.hasItem;

/**
 * @author Vaclav Rechtberger
 */
@RunWith(SerenityRunner.class)
public class AddNewTodo/* extends DriverBase */{
    Actor jeff = Actor.named("Jeff");

    @Managed//(driver = "firefox")
    WebDriver theBrowser;// = driver;

    @Before
    public void jeffCanBrowseTheWeb(){
        givenThat(jeff).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_add_the_first_todo_item() {
        String Digitalize_JTA_vol_1_collection = "Digitize JLA vol 1 collection";
        //---------------------------------------------------------------------------------
        givenThat(jeff).wasAbleTo(StartWith.anEmptyTodoList());
        when(jeff).attemptsTo(AddATodoItem.called(Digitalize_JTA_vol_1_collection));
        then(jeff).should(seeThat(TodoItemsList.displayed(), hasItem(Digitalize_JTA_vol_1_collection)));
    }
}
